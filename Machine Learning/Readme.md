# Machine Learning (ML)

## Books
- **Pattern Recognition and Machine Learning** by Christopher M. Bishop
- **Machine Learning: A Probabilistic Perspective** by Kevin P. Murphy
- **Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow** by Aurélien Géron

## Online Courses
- [Machine Learning](https://www.coursera.org/learn/machine-learning) by Stanford University on Coursera
- [Deep Learning Specialization](https://www.coursera.org/specializations/deep-learning) by Andrew Ng on Coursera
- [Machine Learning A-Z: Hands-On Python & R In Data Science](https://www.udemy.com/course/machinelearning/) on Udemy

## Practice Platforms
- [Kaggle](https://www.kaggle.com/)
- [Google Colab](https://colab.research.google.com/)
- [DrivenData](https://www.drivendata.org/)
