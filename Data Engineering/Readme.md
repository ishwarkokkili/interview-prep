# Data Engineering (DE)

## Books
- **Designing Data-Intensive Applications** by Martin Kleppmann
- **Data Engineering with Python** by Paul Crickard
- **The Data Warehouse Toolkit: The Definitive Guide to Dimensional Modeling** by Ralph Kimball and Margy Ross

## Online Courses
- [Data Engineering on Google Cloud Platform Specialization](https://www.coursera.org/specializations/gcp-data-machine-learning) by Google Cloud on Coursera
- [Data Engineering, Big Data, and Machine Learning on GCP](https://www.coursera.org/specializations/gcp-data-machine-learning) by Google Cloud on Coursera
- [Data Engineering on Azure](https://www.edx.org/professional-certificate/microsoft-azure-data-engineering) by Microsoft on edX

## Practice Platforms
- [DataCamp](https://www.datacamp.com/)
- [Strata Data Conference](https://conferences.oreilly.com/strata)
- [Coursera Projects](https://www.coursera.org/projects)
