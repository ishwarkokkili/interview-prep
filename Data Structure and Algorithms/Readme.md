# Data Structures and Algorithms (DSA)

## Essential Theory for DSA (Text) 

- [Python wife DSA](https://pythonwife.com/introduction-to-data-structures-and-algorithms/) by Python wife
- [OG GFG](https://www.geeksforgeeks.org/learn-data-structures-and-algorithms-dsa-tutorial/) by Geeksforgeeks 
- [Take you Forward](https://takeuforward.org/strivers-a2z-dsa-course/strivers-a2z-dsa-course-sheet-2) by Striver 

*I would suggest you guys to ask GPT and learn the theory but use these tutorials as a refrence as GPT is Prone to hallucinate at times*

## Online Courses/ Youtube Playlists For Complete Theory 

- [Array, LinkedList, Stack, Queues, searching, Sorting, Hashing](https://www.youtube.com/watch?v=f9Aje_cN_CY&t=18683s&pp=ygULRFNBIFB5dGhvbiA%3D) - by Nitish Sir (if you are coming from a Python background this is the best place to start) 
- [DSA with CPP Abdul Bari](https://www.udemy.com/course/datastructurescncpp/) - by GOD Abdul Bari (CPP background, you already Know he is the Best)
- [Coding minutes DSA C++](https://www.udemy.com/course/cpp-data-structures-algorithms-prateek-narang/) by prateek narang or coding minutes (Alternative)
- [Coding minutes level up DSA](https://www.udemy.com/course/cpp-data-structures-algorithms-levelup-prateek-narang/) by prateek narang or coding minutes 
- [Striver A2Z sheet](https://youtube.com/playlist?list=PLgUwDviBIf0oF6QL8m22w1hIDC1vJ_BHz&si=0S0-Sr7dgaIN4a4A) by striver A2Z Sheet    


## Playlists for patterns/problem Solving 

- [Trees](https://www.youtube.com/watch?v=_ANrF3FJm7I&list=PLkjdNRgDmcc0Pom5erUBU4ZayeU9AyRRu) - by Striver Trees
- [Heaps](https://www.youtube.com/playlist?list=PL_z_8CaSLPWdtY9W22VjnPxG30CXNZpI9) - by Aditya Verma 
- [DP](https://www.youtube.com/watch?v=7eLMOE1jnls&list=PLpIkg8OmuX-JhFpkhgrAwZRtukO0SkwAt) - by Legend codestrorywithmik (100% recommended)
- [Graph](https://www.youtube.com/watch?v=5JGiZnr6B5w&list=PLpIkg8OmuX-LZB9jYzbbZchk277H5CbdY) - by Legend codestorywithmik (100% recommended) 

## Practice Platforms 

- [LeetCode](https://leetcode.com/) - Leetcode is like the Holygrain of DSA Prep. you can't ignore it. 
- [seanprasad](https://seanprashad.com/leetcode-patterns/) - Leetcode Patterns for an organised Prep (Patterns >>> problems) 
- [Neetcode IO](https://neetcode.io/practice) - Neetcode 150 (Same kind of resource but a curacted List with Progress Tracking built in) 
- [Striver SDE sheet](https://takeuforward.org/interviews/strivers-sde-sheet-top-coding-interview-problems) - Striver SDE sheet (Very Popular in the market so Included)